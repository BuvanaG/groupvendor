package vendor;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class AbserveVendor {
	@Test
	public void searchVendor() {
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get("https://acme-test.uipath.com/account/login");
		driver.manage().window().maximize();
		driver.findElementById("email").sendKeys("buviganesh.94@gmail.com");
		driver.findElementById("password").sendKeys("Revenge19@");
		driver.findElementById("buttonLogin").click();
		
		//Thread.sleep(30);
		Actions builder = new Actions(driver);
		//WebElement vendor = driver.findElementByXPath("(//*[text()=' Vendors'])");
		//WebElement vendor = driver.findElementByXPath("(//*[@type='button'])[6]");
		WebElement vendor = driver.findElementByXPath("//*[@id='dashmenu']/div[5]/button");
		builder.moveToElement(vendor).perform();
		
		WebDriverWait wait = new WebDriverWait(driver, 20);
		WebElement searchVendor = driver.findElementByLinkText("Search for Vendor");
		wait.until(ExpectedConditions.visibilityOf(searchVendor)).click();
		
		driver.findElementById("vendorTaxID").sendKeys("DE767565");
		driver.findElementById("buttonSearch").click();
		WebElement printTitle = driver.findElementByXPath("//tr[2]/td");
		//WebElement printTitle = driver.findElementByXPath("//*[@class='table']/tbody/tr[2]/td");
		System.out.println(printTitle.getText());
}
}